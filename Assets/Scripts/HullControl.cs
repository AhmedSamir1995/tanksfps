﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HullControl : MonoBehaviour
{
    public float turnSpeed;
    public GameObject Gun;
    public Vector2 gunMinMax;
    public GameObject Target;
    public GameObject ProjectilePrefab;
    public GameObject projectileSpawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetRot;
        targetRot.y=Quaternion.FromToRotation(transform.forward,(-Target.transform.position + transform.position).normalized).eulerAngles.y;
        targetRot.x= Quaternion.FromToRotation(-Gun.transform.up, (-Target.transform.position - Gun.transform.position).normalized).eulerAngles.x;
        float turnDirectionHull = (Input.GetKey(KeyCode.E) ? 1 : 0) - (Input.GetKey(KeyCode.Q) ? 1 : 0);
        TurnHull(targetRot.y);


        float turnDirectionGun = (Input.GetKey(KeyCode.UpArrow) ? 1 : 0) - (Input.GetKey(KeyCode.DownArrow) ? 1 : 0);
        TurnGun(targetRot.x);


    }

    void TurnHull(float x)
    {
        Vector3 target = this.Target.transform.position;
        target.y = transform.position.y;
        float rotation = Quaternion.FromToRotation(transform.forward, (target - transform.position).normalized).eulerAngles.y;
        if (rotation > 180)
            rotation -= 360;
        //print(rotation);
        transform.forward = Vector3.RotateTowards(transform.forward, target - transform.position, turnSpeed*Time.deltaTime,turnSpeed) ;
        //transform.up = Vector3.up;

        //transform.Rotate(Vector3.up * Mathf.MoveTowardsAngle(transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.y + rotation, turnSpeed * Time.deltaTime));
        //transform.Rotate(Vector3.forward * Mathf.Sign(x)* turnSpeed * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.Space))
            Shoot();
    }
    void TurnGun(float x)
    {

        Vector3 target = this.Target.transform.position-Gun.transform.position;
        target = Gun.transform.position + Vector3.ProjectOnPlane(target, transform.right);
        Debug.DrawLine(Gun.transform.position, target);
        if (Vector3.Project(target, transform.forward).normalized != transform.forward|| Vector3.Project(target, transform.forward).sqrMagnitude<1)
            return;
        Gun.transform.forward = Vector3.RotateTowards(Gun.transform.forward, target - Gun.transform.position , turnSpeed * Time.deltaTime,turnSpeed);
        //Gun.transform.up = Gun.transform.position - target ;
        //Gun.transform.Rotate(Vector3.right * turnSpeed * Time.deltaTime * x);
        //Gun.transform.Rotate(Vector3.right * Mathf.MoveTowardsAngle(transform.rotation.eulerAngles.x, x, turnSpeed * Time.deltaTime));
    }

    void Shoot()
    {
        GameObject.Instantiate(ProjectilePrefab, projectileSpawn.transform.position, projectileSpawn.transform.rotation);
    }
}
