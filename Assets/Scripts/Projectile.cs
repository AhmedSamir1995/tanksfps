﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed;
    public Vector3 velocity;
    public float LifeTime;
    public float maxRayDistance;
    Vector3 direction;
    public GameObject particles;
    public float passedTime;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyAll());
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDirection();
        UpdateVelocity();
        UpdatePosition();
        CheckForHit();
        passedTime += Time.deltaTime;
    }

    void OnHit(GameObject hitGameObject, Vector3 position)
    {
        print("HIT");
        transform.position = position;
        particles.transform.position = position;
        particles.GetComponent<ParticleSystem>().Stop();
        particles.GetComponent<ParticleSystem>().Play();
        DisableProjectile();
    }

    void CheckForHit()
    {

        if (Physics.Raycast(transform.position, direction, out RaycastHit hit, (velocity*Time.deltaTime*2).magnitude))
        {
            OnHit(hit.collider.gameObject,hit.point);
        }

        print(hit.collider);
        
    }

    void UpdateDirection()
    {
        direction = transform.forward;
    }

    void UpdateVelocity()
    {
        velocity = direction * speed;
    }
    void UpdatePosition()
    {
        transform.position = transform.position + velocity*Time.deltaTime;
    }

    void DisableProjectile()
    {
        //gameObject.SetActive(false);
        this.enabled = false;
        particles.SetActive(true);
        //Invoke("DestroyAll", 2);

    }

    IEnumerator DestroyAll()
    {
        yield return new WaitUntil(()=>particles.GetComponent<ParticleSystem>().time>1 &&particles.GetComponent<ParticleSystem>().particleCount < 1|| passedTime>LifeTime);
        Destroy(gameObject);
    }
}
