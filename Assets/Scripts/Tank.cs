﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{

    public GameObject Hull;
    public Renderer LeftTrack;
    public Renderer RightTrack;
    public GameObject TankBase;
    public Vector2 maxSpeeds;
    public Vector2 motionVectors;
    public float UVSpeedFactor;
    public float UVRotationFactor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        motionVectors.x = (Input.GetKey(KeyCode.D) ? 1 : 0) - (Input.GetKey(KeyCode.A) ? 1 : 0);
        motionVectors.y = (Input.GetKey(KeyCode.W) ? 1 : 0) - (Input.GetKey(KeyCode.S) ? 1 : 0);

        float offsetLeft =  (maxSpeeds.y * motionVectors.y + maxSpeeds.x * motionVectors.x*(
            motionVectors.y==0? 1 : 
            motionVectors.y > 0? 1
                :0)) * Time.deltaTime;
        float offsetRight = (maxSpeeds.y * motionVectors.y + maxSpeeds.x * motionVectors.x*(
            motionVectors.y==0? -1 : 
            motionVectors.y > 0? 0
                :1)) * Time.deltaTime;
        LeftTrack.material.mainTextureOffset += Vector2.right * offsetLeft;
        RightTrack.material.mainTextureOffset += Vector2.right * offsetRight;

        /*LeftTrack.material.mainTextureOffset += Vector2.right * (maxSpeeds.x * motionVectors.x) * Time.deltaTime;
        RightTrack.material.mainTextureOffset -= Vector2.right * (maxSpeeds.x * motionVectors.x) * Time.deltaTime;*/

        Vector3 forward = TankBase.transform.forward + (TankBase.transform.right * offsetLeft - TankBase.transform.right * offsetRight)*UVRotationFactor;
        TankBase.transform.forward = forward.normalized;
        TankBase.transform.position += forward * motionVectors.y * maxSpeeds.y* UVSpeedFactor;


    }
}
