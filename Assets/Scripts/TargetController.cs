﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    public GameObject Target;
    public GameObject Hull;
    Vector3 oldMousePosition;
    Vector3 deltaMousePosition;
    public float sensitivity;
    // Start is called before the first frame update
    void Start()
    {

        oldMousePosition = Input.mousePosition;
    }

    // Update is called once per frame
    void Update()
    {
        deltaMousePosition = oldMousePosition - Input.mousePosition;
        deltaMousePosition *= sensitivity;
        oldMousePosition = Input.mousePosition;


        Target.transform.RotateAround(Hull.transform.position, Hull.transform.up, -deltaMousePosition.x);
        Target.transform.RotateAround(Hull.transform.position, Vector3.Cross(Hull.transform.position - Target.transform.position, Vector3.up).normalized, deltaMousePosition.y);

    }
}
